const burgerElement = document.querySelector('.burger');
const listElement = document.querySelector('.header__list');
const headerElement = document.querySelector('header');

burgerElement.addEventListener('click', () => {
  burgerElement.classList.toggle('open');
  listElement.classList.toggle('show');
});

document.addEventListener('scroll', () => {
  if (document.body.getBoundingClientRect().y < 0) {
    headerElement.classList.add('scroll');
    return;
  }

  headerElement.classList.remove('scroll');
});

const observer = new IntersectionObserver(
  (entries) => {
    if (entries[0].isIntersecting) {
      entries[0].target.classList.add('slide');
    } else {
      entries[0].target.classList.remove('slide');
    }
  },
  { rootMargin: '-100px' },
);

observer.observe(document.querySelector('p + img'));
