$(document).ready(function () {

    // Clic du burger menu
    $('#burger-menu').click(function () {
        // Si le burger menu a été cliqué
        if ($(this).hasClass('clicked')) {
            // On supprime la class clicked
            $(this).removeClass('clicked');
            // On cache le menu
            $('nav').animate({
                'left': '-330px'
            }, 500);
        }
        else {
            // On ajoute la class clicked
            $(this).addClass('clicked');
            // On affiche le menu
            $('nav').animate({
                'left': 0
            }, 500);
        }
    });

    // Clic sur la croix (fermeture du menu)
    $('.close-menu').click(function () {
        // Si le burger menu a été cliqué
        if ($('#burger-menu').hasClass('clicked')) {
            // On cache le menu
            $('nav').animate({
                'left': '-330px'
            }, 500);

            // On supprime la class clicked sur le burger menu
            $('#burger-menu').removeClass('clicked');
        }
        else {
            // On affiche le menu
            $('nav').animate({
                'left': 0
            }, 500);
        }
    });
});